import requests
import logging
import tempfile
import os
import platform
import ctypes
import subprocess
import time
import shutil
import datetime
import configparser
import traceback
import random

logging.basicConfig(level=logging.DEBUG, filename=os.path.join(os.path.abspath(os.path.dirname(__file__)), "log"),
                    filemode="a")


class WallpaperChanger:
    def __init__(self, config):
        self.config = config
        self.config["APPLICATION"]["save_wallpaper_directory"] = \
            self._abs_path(self.config["APPLICATION"]["save_wallpaper_directory"])

        if self.config["APPLICATION"]["save_wallpaper_directory"] is not None \
                and not os.path.exists(self.config["APPLICATION"]["save_wallpaper_directory"]):
            logging.debug("Creating dir: %s", self.config["APPLICATION"]["save_wallpaper_directory"])
            os.makedirs(self.config["APPLICATION"]["save_wallpaper_directory"])

    @staticmethod
    def _abs_path(path):
        if path is None or not len(path):
            return ""

        os_name = platform.system()

        if os_name == "Windows":
            path = (path if path[0] != '~' else os.path.expanduser(path)) \
                if path != '.' else os.path.abspath(os.path.join(os.path.abspath(os.path.dirname(__file__)), path))
        elif os_name == "Linux":
            path = os.path.expanduser(path) if path == '~' \
                else os.path.abspath(os.path.join(os.path.abspath(os.path.dirname(__file__)), path)) \
                if path[0] != '/' else path

        return os.path.abspath(path)

    def change(self):
        try:
            for i in range(0, int(self.config["APPLICATION"]["try_change_count"])):
                source_name = random.choice(self.config["APPLICATION"]["images_sources"].split(","))

                init_args = dict(self.config.items(source_name))
                init_args.pop("source")

                source_class_instance = self._import_source(self.config[source_name]["source"])(**init_args)

                images_urls = source_class_instance.get_images_urls(**dict(self.config.items(source_name + "_ARGS")))

                if images_urls is None:
                    continue

                image_path = self._download_image(random.choice(images_urls))

                if image_path is None:
                    continue

                self._change_wallpaper(image_path)

                self._save_image(image_path, self.config["APPLICATION"]["save_wallpaper_directory"])

                os.remove(image_path)

                break
        except:
            logging.critical("Some error\n%s", str(traceback.format_exc()))

    @staticmethod
    def _download_image(image_url):
        image_data = requests.get(image_url, stream=True, verify=False)

        logging.debug("Image page loaded with code %d", image_data.status_code)

        if image_data.status_code != 200:
            return None

        status = False

        with tempfile.NamedTemporaryFile(delete=False) as image_file:
            logging.debug("Temp image file path: %s", image_file.name)

            for chunk in image_data.iter_content(1024):
                image_file.write(chunk)

            logging.debug("Image file size is %d", os.stat(image_file.name).st_size)

            if os.stat(image_file.name).st_size > 1024:
                status = True

        if not status:
            os.remove(image_file.name)

        return None if not status else image_file.name

    @staticmethod
    def _change_wallpaper(image_path):
        os_name = platform.system()

        if os_name == "Windows":
            SPI_SETDESKWALLPAPER = 20
            ctypes.windll.user32.SystemParametersInfoW(SPI_SETDESKWALLPAPER, 0, image_path, 3)
        elif os_name == "Linux":
            subprocess.call(["gsettings", "set", "org.gnome.desktop.background", "picture-uri",
                             "file://" + image_path])
            time.sleep(2)

        logging.info("Wallpaper changed")

    @staticmethod
    def _save_image(image_path, save_dir):
        logging.debug("Saving image from: %s", image_path)
        if save_dir is None or not len(save_dir):
            logging.debug("Save is off")
            return

        save_path = os.path.join(save_dir,
                                 "Wallpaper-{0}.jpg".format(datetime.datetime.now().strftime('%Y-%m-%d_%H.%M.%S')))

        logging.debug("Trying to save to: %s", save_path)

        shutil.copy(image_path, save_path)

    @staticmethod
    def _import_source(name):
        components = name.split('.')
        mod = __import__(".".join(components[:-1]), fromlist=[components[len(components) - 1]])
        return getattr(mod, components[len(components) - 1])


def main():
    while True:
        config = configparser.ConfigParser()
        config_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "config")

        if os.path.exists(config_path):
            config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), "config"), encoding="utf-8")
        else:
            if not os.path.exists(os.path.dirname(config_path)):
                os.makedirs(os.path.dirname(config_path))

            config["APPLICATION"] = {"try_change_count": "3", "save_wallpaper": "1",
                                     "save_wallpaper_directory": "~/Wallpapers", "auto_change": "1",
                                     "auto_change_timeout": "300", "images_sources": "WALLHAVEN"}

            config["WALLHAVEN"] = {"module": "wallhavenapi.WallhavenApi.WallhavenApi",
                                   "username": "WallhavenApiTestUser", "password": "kanuwok@divismail.ru"}

            config["WALLHAVEN_ARGS"] = {"resolutions": "", "ratios": "", "sorting": "random", "order": "desc",
                                        "category_general": "1", "category_anime": "1", "category_people": "1",
                                        "purity_sfw": "1", "purity_sketchy": "1", "purity_nsfw": "0", "page": "1"}

            with open(config_path, 'w', encoding="utf-8") as configfile:
                config.write(configfile)

        try:
            wallpaper_changer = WallpaperChanger(config)
        except:
            logging.critical("Config file error\n%s", str(traceback.format_exc()))
            return

        wallpaper_changer.change()

        if int(config["APPLICATION"]["auto_change"]) == 1:
            time.sleep(int(config["APPLICATION"]["auto_change_timeout"]))
        else:
            return

if __name__ == "__main__":
    main()
